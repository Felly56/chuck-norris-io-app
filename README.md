# README #

A  small project with 2 screens to showcase  the MVVM architecture, Navigation Component, Hilt Dependency Injection, Kotlin Coroutines, Coroutine Flow and StateFlow, Retrofit Interaction  with a free API namely https://api.chucknorris.io/ ,  and displaying a few jokes in a RecyclerView, and  ViewPager to be able to swipe through fragments.

# Chuck Norris Jokes App #
This is an an Android App that queries data to Chuck Norris IO API to retrieve and show Chuck Norries Jokes.
In this application, the user is able:

* To View a list of Jokes Categories
* Select and request a random joke for the selected category
* To request different jokes based on selected category

# UI/UX Design #

https://www.figma.com/file/zRLfdoY7xhZFILem3Gx2mF/Chuck-Norris-App


# Architecture # 

### The app is developed using: ###
* 100 % Kotlin
* Model-View-ViewModel (MVVM) architecture
* Single Activity Model
* Navigation Component.
* Hilt Dependency Injection.
* Kotlin Coroutines
* Kotlin Flow and StateFow
* LiveData
* Retrofit
* Glide
* ViewPager2 with TabLayout

* 
# Dependency Injection Mapping #
![Dependency Injecttion Mapping](/images/chucknorris_app_architecture.png)

# Data flow architecture #
![Dependency Injecttion Mapping](/images/chucknorris_app_data_flow.PNG)

# ScreenShots #
![Random Joke Loading](/images/random_joke_loading.png)
# #
![Random Joke](/images/radom_joke.png)
# #
![Joke Category List](/images/joke_list.png)

 
# App GIF #
![Application Gif](/images/chuck_noris_app.gif)


## Who do I talk to? ##
* Repo owner or admin : Felly
* Cellphone: +27 65 715 9671
* Email : fellymwepu@outlook.com